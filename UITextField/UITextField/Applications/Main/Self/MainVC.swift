//
//  ViewController.swift
//  UITextField
//
//  Created by Hong Kheatbung on 7/1/23.
//

import UIKit

class MainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapNormalTextField(_ sender: UIButton) {
            let vc = UIStoryboard(name: "NormalTextFieldSB", bundle: nil).instantiateViewController(withIdentifier: "NormalTextFieldVC") as! NormalTextFieldVC
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapTextFieldWithPod(_ sender: UIButton) {
        let vc = UIStoryboard(name: "TextFieldWithPodSB", bundle: nil).instantiateViewController(withIdentifier: "TextFieldWithPodVC") as! TextFieldWithPodVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapNormalTextView(_ sender: UIButton) {
        let vc = UIStoryboard(name: "NormalTextViewSB", bundle: nil).instantiateViewController(withIdentifier: "NormalTextViewVC") as! NormalTextViewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

