//
//  TextFieldWithPodVC.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import UIKit

class TextFieldWithPodVC: UIViewController {
    
    @IBOutlet weak var tableView    : UITableView!
    @IBOutlet weak var btnDone      : UIButton!
      
    var textFieldWithPodVM = TextFieldWithPodVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Init data from VM
        self.textFieldWithPodVM.initTextFieldWithPod()
        
        // Set disable button to prevent any action
        self.btnDone.isEnabled = false
    }
    
    // Validation for btnDone
    private func doneBtnValidation() {
        // Get Value from VM
        let isUsername          = !self.textFieldWithPodVM.getValue(rowType: .Username).isEmpty         && self.textFieldWithPodVM.getIsValidation(rowType: .Username)
        let isEmail             = !self.textFieldWithPodVM.getValue(rowType: .Email).isEmpty            && self.textFieldWithPodVM.getIsValidation(rowType: .Email)
        let isPassword          = !self.textFieldWithPodVM.getValue(rowType: .Password).isEmpty         && self.textFieldWithPodVM.getIsValidation(rowType: .Password)
        let isConfirmPassword   = !self.textFieldWithPodVM.getValue(rowType: .ConfirmPassword).isEmpty  && self.textFieldWithPodVM.getIsValidation(rowType: .ConfirmPassword)
        
        let isEnable            = isUsername && isEmail && isPassword && isConfirmPassword
        
        self.btnDone.isEnabled          = isEnable
        self.btnDone.backgroundColor    = isEnable ? .blue : .lightGray
        self.btnDone.setTitleColor(isEnable ? .cyan : .darkGray, for: .normal)
    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        let username        = self.textFieldWithPodVM.getValue(rowType: .Username)
        let email           = self.textFieldWithPodVM.getValue(rowType: .Email)
        let password        = self.textFieldWithPodVM.getValue(rowType: .Password)
        let confirmPassword = self.textFieldWithPodVM.getValue(rowType: .ConfirmPassword)
          
        self.customAlert(type       : .CONFIRM_ALERT,
                         image      : "check",
                         title      : "Your Information",
                         message    : "User name: \(username) \n\nEmail: \(email) \n\nPassword: \(password) \n\nConfirm Password: \(confirmPassword)",
                         greyTitle: "Confirm") { (isRedPress) in
            // Back to root
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension TextFieldWithPodVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.textFieldWithPodVM.withPodRec?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldWithPodCell") as! TextFieldWithPodCell
        
        cell.textFieldWithPodVM         = self.textFieldWithPodVM
        cell.textFieldWithPodDataRec    = self.textFieldWithPodVM.withPodRec?[indexPath.row]
        cell.configCell(data: self.textFieldWithPodVM.withPodRec?[indexPath.row])
        
        // Completion from NormalTextFieldCell
        cell.completionRec = {
            
            // Validation for btnDone
            self.doneBtnValidation()
        }
        
        return cell
    }
}
