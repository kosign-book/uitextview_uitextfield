//
//  NormalTextViewModel.swift
//  UITextField
//
//  Created by Hong Kheatbung on 8/1/23.
//

import Foundation

struct NormalTextViewModel {
     
    var object : [NormalObject?]
    
    struct NormalObject {
        var rowType         : RowType?
        var title           : String?
        var value           : String?
        var placeholder     : String?
        var isValidation    : Bool?
    }
    
}
